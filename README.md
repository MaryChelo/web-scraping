# Web scraping con Python
[![](imagenes/logoPython.png)](https://www.python.org/) <br/>
<br/>

Web Scraping (raspado web) es un proceso de automatización en la obtención de datos en sitios web, con Web Scraping los datos de páginas web pueden ser constantemente 'raspados'.  
> En algunos sitios web, el raspado web puede ser ilegal.

<br/>

El objetivo de este repositorio es introducir al Web Scraping en páginas web haciendo uso del lenguaje de programación Python.
Este repositorio tiene dos propósitos:

* Tutorial de introducción en Web Scraping con Pyhon, para asi comenzar con la extracción de contenido en páginas web, **qué** librerías han sido utilizada y con pequeños ejemplos **cómo** lo hacen.
* Mostrar un ejemplo de Web Scraping con la librería BeautifulSoup a la página web [IMDB](https://www.imdb.com/) con el objetivo de analizar las distribuciones de las calificaciones de películas de IMDB utilizando pandas y matplotlib.


#### Tabla de contenido


1. [Web Scraping](#1-web-scraping-) <br/>
    1.1 [Usos](#usos-)
2. [Scraping usando el framework Scrapy](#scrapy-)
3. [Scraping usando el WebDriver Selenium](#selenium-)
4. [Scraping usando la biblioteca BeautifulSoup4](#beautiful-soup-) <br/>
5. [Instalación](#instalaci%C3%B3n-) <br/>
    5.1 [Anaconda](#anaconda)
6. [Documentación](#documentaci%C3%B3n-)
    * [HTML](#html-)
    * [Peticiones](#requests)
7. [Descargar repositorio](#descargar-repositorio)
8. [Pruebas](#pruebas-)
7. [Referencias](#referencias-)

## 1. Web Scraping :star:
Web scraping es un término usado para describir el uso de un programa o algoritmo para extraer y procesar grandes cantidades de datos de la web. El raspado web con Python es una habilidad que puede usar para extraer los datos en una forma útil que se puede importar.
### Usos :dart:
    * Minería de datos.
    * Monitoreo de cambio de precio en línea y comparación de precios.
    * Revisión del producto Scraping: para ver a tu competencia.
    * Recopilación de listados de bienes raíces.
    * Monitoreo de datos del clima.
    * Detección de cambio de sitio web.
    * Investigación.
    * Seguimiento de la presencia y reputación en línea.
    * Integración de datos web.
<br/>

Existen varias maneras de realizar Web Scraping, entre las más conocidas destacan:
* Mediante el framework <code> Scray </code>
* Mediante el Driver <code> Selenium </code>
* Mediante la biblioteca <code> BeautifulSoup </code>

## Scrapy :fast_forward:
Scrapy es un framework de rastreo web gratuito y de código abierto, escrito en Python. Originalmente diseñado para el rastreo web, también se puede utilizar para extraer datos mediante API o como un rastreador web de propósito general. Actualmente es mantenido por [Scrapinghub Ltd](https://scrapinghub.com/), una empresa de desarrollo y servicios de raspado web. <br/>
[Ir a la documentación del repositorio](Scrapy/README.md)

## Selenium :fast_forward:
Selenium es una herramienta de automatización del navegador web.
Le permite abrir un navegador de su elección y realizar tareas como lo haría un ser humano, como:
* Botones de clic
* Ingreso de información en formularios.
* **Buscar información específica en las páginas web.**
WebDriver tiene una API "basada en objetos"; Representamos todo tipo de elementos utilizando la misma interfaz. <br/>

[Ir a la documentación del repositorio](Selenium/README.md)
## Beautiful Soup :fast_forward:
Beautiful Soup es una biblioteca de Python para analizar documentos HTML. Esta biblioteca crea un árbol con todos los elementos del documento y puede ser utilizado para extraer información. Por lo tanto, esta biblioteca es útil para realizar web scraping — extraer información de sitios web. <br/>

[Ir a la documentación del repositorio](BeautifulSoup/README.md)
## Instalación 🔧

Para este proyecto se instaló <code> Anaconda </code> y se trabajó <code> Jupyter Notebook </code>

## Anaconda
[![](imagenes/anaconda.png)](https://www.anaconda.com/) <br />
* Requerimientos de sistema

| Componente        | Características           | 
| ------------- |:-------------:|
| Licencia    | Gratuito | 
| Espacio en disco duro     | mínimo 3 GB    |   
| Sistema Operativo | Windows Vista o más reciente, macOS 10.10+ de 64 bits, o Linux, incluyendo Ubuntu, RedHat, CentOS 6+ y otros.   | 
| Arquitectura del sistema |  x86 de 64 bits, x86 de 32 bits con Windows o Linux, Power8 o Power9.|

* ¿Qué incluye Anaconda?
```
NumPy - Matriz N-dimensional para computación numérica
SciPy - Biblioteca de computación científica para Python
Matplotlib - Librería de gráficos 2D para Python
Pandas - Potentes estructuras de datos de Python.
Scikit-Learn - Módulos Python para aprendizaje automático y minería de datos.
Seaborn - Biblioteca de gráficos estadísticos para Python
Bokeh - Biblioteca interactiva de visualización web
```

![](imagenes/anacondaPantall.png)
<br />

```
Jupyter Notebook - Aplicación web que te permite crear y compartir.
Documentos que contienen código.
```
**Windows**

1. Descargar instalador de Anaconda para [Windows](https://www.anaconda.com/download/#windows).
![](imagenes/instaladorWindows.png)
<br />
2. Haga doble clic en el instalador para iniciar.
> Si tiene problemas durante la instalación, desactive temporalmente su software antivirus durante la instalación y, a continuación, vuelva a habilitarlo una vez que finalice la instalación.
3. Haga clic en Siguiente.
4. Lea los términos de la licencia y haga clic en "Acepto".

**Linux** 
1. Descargar desde la página oficial de [Anaconda](http://docs.anaconda.com/anaconda/install/linux/)

![](imagenes/download1.png)
<br />

![](imagenes/download2.png)
<br />

2. Ejecuta lo siguiente

```bash
md5sum /path/filename

```
3. Anaconda para python 3.7

```bash
bash ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh

```
4. Ejecutar en la terminal

```shell
export PATH=~/anaconda3/bin:$PATH

```
```shell
anaconda-navigator

```
![](imagenes/ejecutandoAnaconda.png)
<br />


**Mac OX**
1. Descargue el instalador gráfico de [MacOS](http://docs.anaconda.com/anaconda/install/mac-os/#macos-graphical-install) para su versión de Python.
2. Haga doble clic en el archivo descargado y haga clic en continuar para iniciar la instalación.
3. Responda las indicaciones en las pantallas Introducción, Léame y Licencia.
4. Haga clic en el botón Instalar para instalar Anaconda en su directorio de usuarios domésticos (recomendado):
5. Ejecutar en la terminal para Python 3.7
```bash
bash ~/Downloads/Anaconda3-5.3.0-MacOSX-x86_64.sh
```
<br/>

## Documentación :paperclip:

Al acceder a una página web el navegador realiza una solicitud <code> GET </code> al servidor web, la respuesta a esta petición son archivos para representar la página web:
* HTML – Contenido principal.
* CSS: Estilo.
* JS - Interactividad a las páginas web. 

<br/>

### HTML ⚙
El lenguaje de marcado de hipertexto es un lenguaje de marcado que le dice a un navegador cómo diseñar el contenido. HTML consiste en elementos llamados [etiquetas](https://developer.mozilla.org/en-US/docs/Web/HTML/Element).
**Etiquetas** <br/>
Se muestran algunos tipos de etiquetas que son utilizadas en HTML:

| Elemento        | Descripción  | 
| ------------- |-------------|
| < html >   | El elemento HTML <code> html </code> representa la raíz (elemento de nivel superior) de un documento HTML, por lo que también se conoce como el elemento raíz. Todos los demás elementos deben ser descendientes de este elemento. | 
| < link >   | El elemento HTML External Resource Link (<code> link </code>) especifica las relaciones entre el documento actual y un recurso externo.| 
| <  meta >   |  El elemento HTML <code> meta </code> representa metadatos que no pueden ser representados por otros elementos relacionados con HTML, como < base >, < link >, < script >, < style > o < title >. | 
| < style >   | El elemento HTML <code> style </code> contiene información para un documento, o parte de un documento  | 
| < title >   | El elemento Título de HTML (<code> title</code>) define el título del documento que se muestra en la barra de título de un navegador o en la pestaña de una página. | 
| < body >   |  El elemento HTML <code> body </code> representa el contenido de un documento HTML. Solo puede haber un elemento <body> en un documento. | 
| < h1 > - < h6 >   | Los elementos HTML <code> h1 </code> - <code> h6 </code>  representan seis niveles de encabezados de sección.|
| < nav >   | El elemento HTML <code> nav </code> representa una sección de una página cuyo propósito es proporcionar enlaces de navegación, ya sea dentro del documento actual o a otros documentos.|
| < div >   | El elemento de división de contenido HTML (<code> div </code>) es el contenedor genérico para el contenido de flujo. No tiene ningún efecto sobre el contenido o el diseño hasta que se diseña usando CSS.|
| < a >   | El elemento HTML <code> a </code> (o elemento de anclaje) crea un hipervínculo a otras páginas web, archivos, ubicaciones dentro de la misma página, direcciones de correo electrónico o cualquier otra URL.|
| < span >   |El elemento HTML <code> span </code> es un contenedor genérico en línea para redactar contenido, que no representa de forma inherente nada. Se puede usar para agrupar elementos con fines de estilo (usando los atributos de clase o id), o porque comparten valores de atributos, como lang.|
| < strong >   |El Strong HTML Element of Importance (<code> strong </code>) indica que su contenido tiene una importancia, seriedad o urgencia fuertes. Los navegadores suelen mostrar los contenidos en negrita. |

### Requests
Las [peticiones](http://docs.python-requests.org/en/master/) es la única biblioteca HTTP sin OMG para Python, segura para humanos.
Las solicitudes le permiten enviar solicitudes HTTP / 1.1, sin la necesidad de trabajo manual. No hay necesidad de agregar manualmente cadenas de consulta a sus URL, o de codificar de forma sus datos POST. 

```shell
$ pipenv install requests
```
> Si usas Anaconda no se necesita instalar.

<br />

[![](imagenes/capturaCodigo/request.png)](https://www.python.org/) 

<code> urllib  </code> es un módulo integrado en la biblioteca estándar de Python y utiliza http.client que implementa el lado del cliente de los protocolos HTTP y HTTPS. No es necesario instalar pip porque urllib se distribuye e instala con Python.

## Descargar repositorio
* Para obtener este tutorial puedes realizar un FORK, clonarlo o descargar el ZIP.


## Pruebas ⚙
* Para probar la funcionalidad de Web Scraping se realizó la implementación del [ejemplo](https://www.dataquest.io/blog/web-scraping-beautifulsoup/) para la obtención de datos en la página web IMDB.
* Solo necesitas descargar e instalar Anaconda y correr Jupyter Notebook para ejecutar este ejemplo.
* Da clic aquí para [ir al código](Ejemplos/BeautifulSoup-Movies.ipynb)

<br/><br/>

## Referencias :pushpin:
* https://developer.mozilla.org/en-US/docs/Web/HTML/Element
* https://www.twilio.com/blog/2016/12/http-requests-in-python-3.html